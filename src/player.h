#ifndef PLAYER_H
#define PLAYER_H

#define MAX_GRAVITY 31
#define MAX_WALK_SPEED 31

#include <SFML/Graphics.hpp>

namespace player {

	void init();
	
	void goToTile(int, int);
	
	void update();
	sf::Sprite& getSprite();
	
	int getCloudSpeed();
	
	void setGravity(int);
	int getGravity();
	
	void setFriction(int);
	int getFriction();
	
	void setJumpStrength(int);
	int getJumpStrength();
	
	void setWalkSpeed(int);
	int getWalkSpeed();
	
	void setDifficulty(int);
	int getDifficulty();
	
	int getX();
	int getY();
	
	void setGender(char, bool);
	bool getGender(char);
	bool isMale();
	bool isFemale();
}


#endif
