#ifndef POWERUP_H
#define POWERUP_H

#include "settings.h"

#include <SFML/Graphics.hpp>

class Powerup {
	public:
		int getX() const { return x; }
		int getY() const { return y; }
		settings::settingName getSetting() const { return setting; }
		
		Powerup(int _x, int _y, settings::settingName _name);
		
		sf::Sprite& getSprite();

	private:
		int x;
		int y;
		sf::Sprite sprite;
		settings::settingName setting;
};

#endif
