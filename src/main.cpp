#include <thread>
#include <iostream>

#include "display.h"
#include "player.h"
#include "world.h"
#include "audio.h"
#include "settings.h"
#include "io.h"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

bool gameOpen = true;

const int ACTION_BUTTON = sf::Keyboard::Space;
const int OTHER_ACTION_BUTTON = sf::Keyboard::Tab;

int main() {

	settings::init();
	audio::init();
	world::init();
	display::init();
	player::init();

	sf::RenderWindow window(sf::VideoMode(TOTALWIDTH + SETTINGSWIDTH, TOTALHEIGHT), "creecher game");

	while(gameOpen) {
		std::this_thread::sleep_for(std::chrono::milliseconds(1000 / display::getFps()));
		sf::Event event;
		while(window.pollEvent(event)) {
			const std::unordered_set<Button*>& visibleButtons = settings::listButtons();
			switch(event.type) {

				case sf::Event::MouseMoved: {
					for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr){
						if((*itr)->rect.contains(event.mouseMove.x / display::getGlobalScale(), event.mouseMove.y / display::getGlobalScale())){
							(*itr)->highlight();
						} else {
							(*itr)->reset();
						}
					}
					break;
				}
				
				case sf::Event::MouseButtonPressed: {
					for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr){
						if((*itr)->rect.contains(event.mouseButton.x / display::getGlobalScale(), event.mouseButton.y / display::getGlobalScale())){
							(*itr)->depress();
						} else {
							(*itr)->reset();
						}
					}
					break;
				}
				
				case sf::Event::JoystickButtonPressed: {
					std::cout << "Pressed button " << event.joystickButton.button << " on joystick " << event.joystickButton.joystickId << std::endl;
					
					switch(event.joystickButton.button) {
						case 0:
						case 6:
							io::onInputDown(io::inputKey::jump);
							break;
					}
					break;
				}
				
				case sf::Event::JoystickButtonReleased: {
					switch(event.joystickButton.button) {
						case 0:
						case 6:
							io::onInputUp(io::inputKey::jump);
							break;
					}
					break;
				}
				
				case sf::Event::JoystickMoved: {
					switch(event.joystickMove.axis) {
						case sf::Joystick::Axis::X:
						case sf::Joystick::Axis::PovX:
							if (event.joystickMove.position < 0) {
								io::onInputDown(io::inputKey::left);
							} else {
								io::onInputUp(io::inputKey::left);
							}
							if (event.joystickMove.position > 0) {
								io::onInputDown(io::inputKey::right);
							} else {
								io::onInputUp(io::inputKey::right);
							}
							break;
						case sf::Joystick::Axis::Y:
						case sf::Joystick::Axis::PovY:
							if (event.joystickMove.position < 0) {
								io::onInputDown(io::inputKey::up);
							} else {
								io::onInputUp(io::inputKey::up);
							}
							if (event.joystickMove.position > 0) {
								io::onInputDown(io::inputKey::down);
							} else {
								io::onInputUp(io::inputKey::down);
							}
							break;
					}
					break;
				}
				
				case sf::Event::MouseButtonReleased: {
				
					if (event.mouseButton.x / display::getGlobalScale() > SETTINGSWIDTH) {
						if (player::getDifficulty() == 1) {
						
							int mouseX = (event.mouseButton.x / display::getGlobalScale() - SETTINGSWIDTH) / TILESIZE;
							int mouseY = event.mouseButton.y / TILESIZE / display::getGlobalScale();
						
							if (event.mouseButton.button == sf::Mouse::Button::Right) {
								world::setTerrainAt(mouseX, mouseY, SPIKE);
							} else {
								world::setTerrainAt(mouseX, mouseY, !(world::getTerrainAt(mouseX, mouseY)));
							}
						}
					} else {
						for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr){
							Button* currentButton;
							currentButton = *itr;
							if(currentButton->rect.contains(event.mouseButton.x / display::getGlobalScale(), event.mouseButton.y / display::getGlobalScale()) && !currentButton->isDisabled()){
								currentButton->effect();
								currentButton->reset();
								currentButton->highlight();
								break;
							}
						}
					}
					break;
				}
			
				case sf::Event::Closed: {
					gameOpen = false;
					break;
				}
				
				case sf::Event::KeyPressed: {
					io::keyDown(event.key.code);
					break;
				}
				
				case sf::Event::KeyReleased: {
					io::keyUp(event.key.code);
					break;
				}
			}
		}
		player::update();
		display::update(window);
	}
}
