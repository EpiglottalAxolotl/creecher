#include "io.h"

#include <map>
#include <unordered_set>

#include <SFML/Window.hpp>

namespace player {
	void onActionButton();
}

namespace io {

	std::unordered_set<inputKey> keys;

	void onInputDown(inputKey ik) {
		keys.insert(ik);
		if (ik == inputKey::jump) player::onActionButton();
	}
	
	void onInputUp(inputKey ik) {
		keys.erase(ik);
	}
	
	void keyDown(int keyCode) {
		switch (keyCode) {
			case sf::Keyboard::W:
				onInputDown(inputKey::up);
				break;
			case sf::Keyboard::A:
				onInputDown(inputKey::left);
				break;
			case sf::Keyboard::S:
				onInputDown(inputKey::down);
				break;
			case sf::Keyboard::D:
				onInputDown(inputKey::right);
				break;
			case sf::Keyboard::Space:
				onInputDown(inputKey::jump);
				break;
		}
	}

	void keyUp(int keyCode) {
		switch (keyCode) {
			case sf::Keyboard::W:
				onInputUp(inputKey::up);
				break;
			case sf::Keyboard::A:
				onInputUp(inputKey::left);
				break;
			case sf::Keyboard::S:
				onInputUp(inputKey::down);
				break;
			case sf::Keyboard::D:
				onInputUp(inputKey::right);
				break;
			case sf::Keyboard::Space:
				onInputUp(inputKey::jump);
				break;
			}
	}
	
	bool isButtonPressed(inputKey ik) {
		return keys.count(ik) > 0;
	}
}
