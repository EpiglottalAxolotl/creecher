#ifndef AUDIO_H
#define AUDIO_H

#define MAX_VOLUME 20
#define MAX_PITCH 12

namespace audio {

	enum soundEffect {
		jump, death, bounce, unlock, SFX_MAX
	};

	void init();
	void playSound(soundEffect);
	void setMusicVolume(int);
	void setSoundVolume(int);
	void setMusicPitch(int);
	
	int getMusicVolume();
	int getSoundVolume();
	int getMusicPitch();
	
	void startMusic();
	void stopMusic();
}

#endif
