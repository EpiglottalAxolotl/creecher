#include "audio.h"

#include <cmath>

#include <SFML/Audio.hpp>

namespace audio {
	sf::Music music;
	sf::SoundBuffer buffers[SFX_MAX];
	sf::Sound sounds[SFX_MAX];
	
	std::string sfxNames[SFX_MAX] = {
		"jump", "death", "bounce", "unlock"
	};
	
	int musicVolume = 15;
	int soundVolume = 19;
	int musicPitch = 0;
	
	void init() {
		music.openFromFile("assets/embeepened.ogg");
		music.setVolume(musicVolume * 100 / MAX_VOLUME);
		music.setLoop(true);
		music.play();
		
		for(int i=0; i<SFX_MAX; ++i) {
			buffers[i].loadFromFile("assets/" + sfxNames[i] + ".wav");
			sounds[i].setBuffer(buffers[i]);
		}
	}
	
	void playSound(soundEffect effect) {
		sounds[effect].setVolume(soundVolume * 100 / MAX_VOLUME);
		sounds[effect].play();
	}
	
	void setMusicVolume(int v) {
		if (v<0 || v>MAX_VOLUME) return;
		musicVolume = v;
		music.setVolume(v * 100 / MAX_VOLUME);
	}
	
	void setSoundVolume(int v) {
		if (v<0 || v>MAX_VOLUME) return;
		soundVolume = v;
	}
	
	int getMusicVolume() {
		return musicVolume;
	}
	
	int getSoundVolume() {
		return soundVolume;
	}
	
	int getMusicPitch() {
		return musicPitch;
	}
	
	void setMusicPitch(int p) {
		musicPitch = p;
		music.setPitch(std::pow(2, p/12.0));
	}
	
	void startMusic() {
		music.play();
	}
	
	void stopMusic() {
		music.stop();
	}
}
