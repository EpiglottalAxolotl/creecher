#include <unordered_set>
#include <iostream>

#include "player.h"
#include "display.h"
#include "world.h"
#include "audio.h"
#include "io.h"

#include <climits>

namespace world {
	bool terrainIsSolid(int, int);
	bool isSolid(unsigned char);
	bool isSpike(int, int);
}

namespace player {

	int x;
	int y;
	int velX;
	int velY;
	int velXMax = TILESIZE - 1;
	int velYMax = TILESIZE - 1;
	
	int jump = 32;
	int gravity = 4;
	int speed = 4;
	int friction = 3;
	
	int difficulty = 2;
	
	bool male = false;
	bool female = false;
	
	int getJumpStrength() {
		return jump;
	}
	
	void setJumpStrength(int j) {
		jump = j;
	}
	
	int getGravity() {
		return gravity;
	}
	
	void setGravity(int g) {
		gravity = g;
	}
	
	int getWalkSpeed() {
		return speed;
	}
	
	void setWalkSpeed(int w) {
		speed = w;
	}
	
	int getFriction() {
		return friction;
	}
	
	void setFriction(int f) {
		friction = f;
	}
	
	int getDifficulty() {
		return difficulty;
	}
	
	void setDifficulty(int d) {
		if (difficulty == 3) audio::startMusic();
		difficulty = d;
		if (difficulty == 3) audio::stopMusic();
	}
	
	bool canJump = true;
	
	sf::Sprite sprite;
	
	int getX() {
		return x;
	}
	int getY() {
		return y;
	}
	
	void setGender(char gender, bool set) {
		switch(gender) {
			case 'm':
				male = set;
				break;
			case 'f':
				female = set;
				break;
		}
	}
	
	bool getGender(char gender) {
		switch(gender) {
			case 'm': return isMale();
			case 'f': return isFemale();
		}
		return false;
	}
	
	bool isMale() {
		return male;
	}
	
	bool isFemale() {
		return female;
	}

	void onActionButton() {
		// old airdash
		/*
		velX = 0;
		velY = 0;
		if (keys.count(sf::Keyboard::W)) velY -= jump;
		if (keys.count(sf::Keyboard::A)) velX -= jump;
		if (keys.count(sf::Keyboard::S)) velY += jump;
		if (keys.count(sf::Keyboard::D)) velX += jump;
		*/
		
		if (canJump) {
			canJump = false;
			velY += gravity < 0 ? jump : -jump;
			audio::playSound(audio::soundEffect::jump);
		}
	}
	
	void onOtherActionButton() {
		world::setTerrainAt(x / TILESIZE, y / TILESIZE, 1);
	}

	void init() {
		sprite.setTexture(display::getTileset());
		sprite.setTextureRect(sf::IntRect(7*TILESIZE, 7*TILESIZE, TILESIZE, TILESIZE));
	}
	
	void goToTile(int tileX, int tileY) {
		x = tileX * TILESIZE;
		y = tileY * TILESIZE;
		velX = 0;
		velY = 0;
	}
	
	void onDeath() {
		audio::playSound(audio::soundEffect::death);
		goToTile(world::getSpawnX(), world::getSpawnY());
	}

	void collide() {
		bool debug = false;

		int debug_original_x = x;
		int debug_original_y = y;
		int debug_original_edgeH;
		int debug_original_edgeV;
		int debug_original_velX = velX;
		int debug_original_velY = velY;
		
		if (x % TILESIZE == 0 && y % TILESIZE == 0 && velX != 0 && velY != 0) {
			std::cout << "Warning: Aligned to grid and both velocities nonzero. Glitch may occur." << std::endl;
			debug = true;
		}

		// The coordinates of the grid spaces containing the corners.
		int leftX = x / TILESIZE;
		int rightX = (x % TILESIZE == 0 && velX <= 0) ? leftX : leftX + 1;
		int topY = y / TILESIZE;
		int bottomY = (y % TILESIZE == 0 && velY <= 0) ? topY : topY + 1;
		
		int xSign = (velX > 0) - (velX < 0);
		int ySign = (velY > 0) - (velY < 0);

		int edgeH; // The first horizontal gridline that will be intersected by the trajectory.
		if (velY > 0) {
			edgeH = y / TILESIZE + 1;
		} else {
			edgeH = y / TILESIZE;
		}

		int edgeV; // The first vertical gridline that will be intersected by the trajectory.
		if (velX > 0) {
			edgeV = x / TILESIZE + 1;
		} else {
			edgeV = x / TILESIZE;
		}
		
		debug_original_edgeH = edgeH;
		debug_original_edgeV = edgeV;

		bool bounce = false;
		int bounceX = 0;
		int bounceY = 0;

		// Special case for hitting a corner directly:
		if (x % TILESIZE == 0 && y % TILESIZE == 0 && velX != 0 && velY != 0) {
			
			unsigned char cornerBlock = world::getTerrainAt(leftX + xSign, topY + ySign);
			if (world::isSolid(cornerBlock)) {
				if (abs(velX) < abs(velY)) {
					if(cornerBlock == BOUNCY) bounceX = -velX;
					velX = 0;
				} else {
					if(cornerBlock == BOUNCY) bounceY = -velY;
					velY = 0;
				}
			}
		}

		// If we are perfectly aligned to the grid on one axis, the leading edge will cross one more gridline than the trailing edge does. This is why we have to conditionally add TILESIZE-1 to this calculation.
		// Currently, this has a problem: it will go until x and y have each crossed at least one gridline, I think?
		// On the one hand, taking out the conditionally-adding-tilesize prevents the current jumping bug. On the other hand, it leads to clipping through the floor...
		// I think the solution is to remove this and find a different solution for the floorclipping.
		while ( (velX && xSign * (x + velX) >= xSign * edgeV * TILESIZE) || (velY && ySign * (y + velY) >= ySign * edgeH * TILESIZE ) ) {

			int distanceToVert = velX == 0 ? INT_MAX : abs((edgeV * TILESIZE - x) * velY);
			int distanceToHorz = velY == 0 ? INT_MAX : abs((edgeH * TILESIZE - y) * velX);
			
			if (debug) std::cout << "Distance to vertical: " << distanceToVert << "; distance to horizontal: " << distanceToHorz << std::endl;
			
			bool horiz;

			if (velY == 0 || distanceToVert < distanceToHorz) { // The first gridline intersected by the vector is vertical
				if (debug) std::cout << "Crossing vertical axis" << std::endl;
				leftX += xSign;
				rightX += xSign;
				
				horiz = false;
			} else {
				if (debug) std::cout << "Crossing horizontal axis" << std::endl;
				topY += ySign;
				bottomY += ySign;
				
				horiz = true;
			}
			
			unsigned char terrainTL = world::getTerrainAt(leftX, topY);
			unsigned char terrainTR = world::getTerrainAt(rightX, topY);
			unsigned char terrainBL = world::getTerrainAt(leftX, bottomY);
			unsigned char terrainBR = world::getTerrainAt(rightX, bottomY);
			
			if(terrainTL == BOUNCY || terrainTR == BOUNCY || terrainBL == BOUNCY || terrainBR == BOUNCY) {
				bounce = true;
			}
			if (world::isSolid(terrainTL) || world::isSolid(terrainTR) || world::isSolid(terrainBL) || world::isSolid(terrainBR)) {
				if (horiz) {
					int newY = (ySign > 0 ? bottomY - 1 : topY + 1) * TILESIZE;
					if (ySign * newY > ySign * (y + velY)) {
						std::cout << "ERROR! delta y greater than velocity? Trying to move from " << x << "," << y << " to y=" << newY << " when velocity is " << velX << "," << velY << std::endl;
					}
					if (bounce) {
						bounceY = -velY;
					}
					y = newY;
					velY = 0;
					ySign = 0;
				} else {
					int newX = (xSign > 0 ? rightX - 1 : leftX + 1) * TILESIZE;
					if (xSign * newX > xSign * (x + velX)) {
						std::cout << "ERROR! delta x greater than velocity? Trying to move from " << x << "," << y << " to x=" << newX << " when velocity is " << velX << "," << velY << std::endl;
					}
					if (bounce) {
						bounceX = -velX;
					}
					x = newX;
					velX = 0;
					xSign = 0;
				}
			} else {
				if (horiz) {
					edgeH += ySign;
				} else {
					edgeV += xSign;
				}
			}

		}
		
		x += velX;
		y += velY;

		if (bounceX != 0) velX = bounceX;
		if (bounceY != 0) velY = bounceY;

		if ( world::isSolid(world::getTerrainAt(x/TILESIZE, y/TILESIZE)) ) {
			std::cout << "ERROR! top left corner is inside solid block!" << std::endl;
			
			std::cout << "Debug information: current x " << x
					<< "; current y " << y
					<< "; initial x " << debug_original_x
					<< "; initial y " << debug_original_y
					<< "; initial velX " << debug_original_velX
					<< "; initial velY " << debug_original_velY
					<< "; initial edgeH " << debug_original_edgeH
					<< "; initial edgeV " << debug_original_edgeV
					<< "; current edgeH " << edgeH
					<< "; current edgeV " << edgeV
					<< std::endl;
		}
		if ( x % TILESIZE && world::isSolid(world::getTerrainAt(x/TILESIZE + 1, y/TILESIZE)) ) {
			std::cout << "ERROR! top right corner is inside solid block!" << std::endl;
		}
		if ( y % TILESIZE && world::isSolid(world::getTerrainAt(x/TILESIZE, y/TILESIZE + 1)) ) {
			std::cout << "ERROR! bottom left corner is inside solid block!" << std::endl;
			
			std::cout << "Debug information: current x " << x
					<< "; current y " << y
					<< "; initial x " << debug_original_x
					<< "; initial y " << debug_original_y
					<< "; initial velX " << debug_original_velX
					<< "; initial velY " << debug_original_velY
					<< "; initial edgeH " << debug_original_edgeH
					<< "; initial edgeV " << debug_original_edgeV
					<< "; current edgeH " << edgeH
					<< "; current edgeV " << edgeV
					<< std::endl;
		}
		if ( x % TILESIZE && y % TILESIZE && world::isSolid(world::getTerrainAt(x/TILESIZE + 1, y/TILESIZE + 1)) ) {
			std::cout << "ERROR! bottom right corner is inside solid block!" << std::endl;
		}
	}

	void update() {
	
		// Check for intersecting spikes
		
		if (world::isSpike(x / TILESIZE, y / TILESIZE)
			|| (x%TILESIZE && world::isSpike(x / TILESIZE + 1, y / TILESIZE) )
			|| (y%TILESIZE && world::isSpike(x / TILESIZE, y / TILESIZE + 1) )
			|| (x%TILESIZE && y%TILESIZE && world::isSpike(x / TILESIZE + 1, y / TILESIZE) )) {
			onDeath();
		}
		
		world::checkPowerups(x, y);
	
		// Update velocities
		
		bool againstFloor =     !(y%TILESIZE) && (world::terrainIsSolid(x / TILESIZE, y / TILESIZE + 1) || (x%TILESIZE && world::terrainIsSolid(x / TILESIZE + 1, y / TILESIZE + 1)));
		bool againstCeiling =   !(y%TILESIZE) && (world::terrainIsSolid(x / TILESIZE, (y-1) / TILESIZE) || (x%TILESIZE && world::terrainIsSolid(x / TILESIZE + 1, (y-1) / TILESIZE)));
		bool againstLeftWall =  !(x%TILESIZE) && (world::terrainIsSolid((x-1) / TILESIZE, y / TILESIZE) || (y%TILESIZE && world::terrainIsSolid((x-1) / TILESIZE, y / TILESIZE + 1)));
		bool againstRightWall = !(x%TILESIZE) && (world::terrainIsSolid(x / TILESIZE + 1, y / TILESIZE) || (y%TILESIZE && world::terrainIsSolid(x / TILESIZE + 1, y / TILESIZE + 1)));
	
		// Friction	
		if (velX > 0) {
			velX = (velX - friction < 0) ? 0 : velX - friction;
		} else if (velX < 0) {
			velX = (velX + friction > 0) ? 0 : velX + friction;
		}
	
		// Motive force
		if (!againstLeftWall  && io::isButtonPressed(io::inputKey::left)) velX -= speed;
		if (!againstRightWall && io::isButtonPressed(io::inputKey::right)) velX += speed;

		// Gravity
		int tempGravity = gravity;
		if ((world::getTerrainAt( (x + TILESIZE/2) / TILESIZE, (y + TILESIZE/2) / TILESIZE ) & 0xf) == 0x4) tempGravity *= -1;
		if (tempGravity<0 ? !againstCeiling : !againstFloor) velY += tempGravity;
		else canJump = true;

		collide();
		
		if (x >= world::getWidth() * TILESIZE || y >= world::getHeight() * TILESIZE || x < 0 || y < 0) onDeath();
	}
	
	sf::Sprite& getSprite() {
		//sprite.setPosition(x + SETTINGSWIDTH,y);
		return sprite;
	}
}
