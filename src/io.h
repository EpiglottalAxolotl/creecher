#ifndef IO_H
#define IO_H

namespace io {

	enum inputKey {
		left, right, up, down, jump, _INPUT_COUNT
	};

	void onInputDown(inputKey);
	void onInputUp(inputKey);
	
	bool isButtonPressed(inputKey);
	
	void keyDown(int);
	void keyUp(int);
	void onActionButton();
	void onOtherActionButton();
}

#endif
