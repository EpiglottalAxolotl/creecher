#ifndef SETTINGS_H
#define SETTINGS_H

#include <functional>
#include <unordered_set>

#include <SFML/Graphics.hpp>

class Button {
	public:
		Button(int screenX, int screenY, int tilesetX, int tilesetY, int width, int height);
	
		sf::IntRect rect;
		
		void highlight();
		void depress();
		void reset();		
		
		void disable(bool);
	
		sf::Sprite* getSprite();
		
		bool isDisabled() const {return _disabled;};
		
		std::function<void()> effect = [](){};
		
	private:
		sf::Sprite* sprite;
		
		//void (*effect)();
		bool _disabled;
		bool _pressed;
		
		int _x;
		int _y;
		
		sf::IntRect _defaultSlice;
		sf::IntRect _highlightSlice;
		sf::IntRect _disabledSlice;
		sf::IntRect _pressedSlice;
};

namespace settings {

	enum settingsPage {
		main, graphics, audio, physics, misc
	};
	
	enum settingName {
		resolution, fps, font,
		soundVolume, musicVolume, musicPitch,
		gravity, friction, jumpStrength, walkSpeed, windSpeed,
		levelSelect, difficulty, month, gender
	};

	void init();

	const std::unordered_set<Button*>& listButtons();
	
	settingsPage getSettingsPage();
	void setPage(settingsPage);
	std::string getPageName();
	
	void onSetGender(char, bool);
	
	void renderSettings();
	
	enum boundsMode {
		unbounded, min, minMax, wrapping
	};
	
	class Setting {
		public:
			virtual void setVisible(bool) = 0;
			virtual void render() const = 0;
			settingsPage getCategory() const { return category; }
		
		protected:
			int yStart;
			std::string displayName;
			settingsPage category;
	};
	
	class IntSetting : public Setting {
		public:
			IntSetting(settingsPage, int(), void(int), int, int, std::string, boundsMode, int min=0, int max=0);
		
			void adjust(int);
			std::function<void(int)> setWrapped;
			std::function<int()> getWrapped;
			
			void setVisible(bool);
			
			virtual void render() const;
		
		private:
			int min;
			int max;
			Button up_b;
			Button dn_b;
			boundsMode mode;
	};
	
	class StringListSetting : public IntSetting {
		public:
			// boundsMode here should only be minmax or wrapping
			StringListSetting(settingsPage, int(), void(int), int, int, std::string, boundsMode, std::vector<std::string>);
			
			void render() const;
			
		private:
			std::vector<std::string> optionNames;
	
	};
	
	struct CheckGroupItem {
		Button on_b;
		Button off_b;
		std::string displayName;
		int pos;
		
		CheckGroupItem(Button on, Button off, std::string name, int _pos) : on_b(on), off_b(off), displayName(name), pos(_pos) { }
	};
	
	class CheckGroupSetting : public Setting {
		public:
			CheckGroupSetting(settingsPage, bool(char), void(char,bool), int, std::string, std::vector<std::pair<char, std::string>>);
			
			void set(char, bool);
			std::function<void(char, bool)> setWrapped;
			std::function<bool(char)> getWrapped;
			
			void setVisible(bool);
			void render() const;

		private:
			std::map<char, CheckGroupItem> items;
	};
	
	void enableSetting(settingName);
}

#endif
