#include "world.h"
#include "settings.h"
#include "audio.h"
#include "player.h"
#include "display.h"

#include <map>

#define MAX_PAGE 5

namespace settings {

	std::vector<std::string> pageNames {
		"SETTINGS",
		"Graphics Settings",
		"Audio Settings",
		"Physics Settings",
		"Other Settings"
	};

	sf::Texture buttonsTexture;
	
	settingsPage page;
	
	std::string getPageName() {
		return pageNames[page];
	}
	
	std::unordered_set<Button*> visibleButtons;
	std::unordered_set<Setting*> visibleSettings;

	// TODO make these shared_ptr eventually	
	std::map<settingsPage, std::vector<Setting*>> pages; // in the future, maybe remove that star
	std::map<settingName, Setting*> settingsCache = {

		// Graphics page		
		{ resolution, 
			new IntSetting(graphics, display::getGlobalScale, display::setGlobalScale, 96, 112, "Resolution (Scale)", min, 1)
		},
		
		{ fps,
			new IntSetting(graphics, display::getFps, display::setFps, 160, 112, "FPS", minMax, 0, 1000)
		},
		
		{ font,
			new StringListSetting(graphics, display::getFont, display::setFont, 224, 192, "Font", wrapping, std::vector<std::string> {
				"Standard",
				"Blackletter",
				"Scribble",
				"Symbol"
			})
		},
		
		// Audio page
		{ soundVolume,
			new IntSetting(audio, audio::getSoundVolume, audio::setSoundVolume,  96, 112, "Sound Volume", minMax,   0, 20)
		},
		{ musicVolume, 
			new IntSetting(audio, audio::getMusicVolume, audio::setMusicVolume, 160, 112, "Music Volume", minMax,   0, 20)
		},
		{ musicPitch, 
			new IntSetting(audio, audio::getMusicPitch,  audio::setMusicPitch,  224, 112, "Music Pitch",  minMax, -12, 12)
		},
		
		// Physics page
		{ gravity,
			new IntSetting (physics, player::getGravity,      player::setGravity,       96, 112, "Gravity",       unbounded)
		},
		{ friction,
			new IntSetting (physics, player::getFriction,     player::setFriction,     160, 112, "Friction",      min, 0)
		},
		{ jumpStrength,
			new IntSetting (physics, player::getJumpStrength, player::setJumpStrength, 224, 112, "Jump Strength", min, 0)
		},
		{ walkSpeed,
			new IntSetting (physics, player::getWalkSpeed,    player::setWalkSpeed,    288, 112, "Walk Speed",    minMax, 0, 31)
		},
		{ windSpeed,
			new IntSetting (physics, display::getWindSpeed,   display::setWindSpeed,   352, 112, "Wind Speed",    unbounded)
		},
		
		// Misc page
		{ levelSelect,
			new IntSetting(misc, world::getLevel, world::loadLevel, 96, 112, "Level", minMax, 0, 5)
		},
		{ difficulty,
			new StringListSetting(misc, player::getDifficulty, player::setDifficulty, 160, 192, "Difficulty", minMax, std::vector<std::string> {
				"Extremely Easy",
				"Easy",
				"Normal",
				"Impossible"
			})
		},
		{ month,
			new StringListSetting(misc, display::getMonth, display::setMonth, 224, 112, "Month", wrapping, std::vector<std::string> {
				"Jan",
				"Feb",
				"Mar",
				"Apr",
				"May",
				"Jun",
				"Jul",
				"Aug",
				"Sep",
				"Oct",
				"Nov",
				"Dec"
			})
		},
		{ gender,
			new CheckGroupSetting(misc, player::getGender, player::setGender, 288, "Gender", std::vector<std::pair<char, std::string>> {
				std::make_pair('m', "Male"),
				std::make_pair('f', "Female")
			})
		}
		
	};
	
	std::vector<Button> pageButtons;
	
	void init() {
		buttonsTexture.loadFromFile("assets/buttons.png");
		
		pageButtons.push_back(Button(24, 24, 0, 0, TILESIZE, TILESIZE));
		pageButtons[0].effect = [](){settings::setPage(settings::settingsPage::main);};
		for (int i=1; i<MAX_PAGE; ++i) {
			pageButtons.push_back(Button(48*i, 48, (i+1)*TILESIZE, 0, TILESIZE, TILESIZE));
			pageButtons[i].effect = [i](){settings::setPage((settings::settingsPage) i);};
		}
		
		pages.insert(std::make_pair(settingsPage::audio, std::vector<Setting*> {
			settingsCache[soundVolume],
			settingsCache[musicVolume],
//			&musicPitch
		}));
		
		pages.insert(std::make_pair(settingsPage::graphics, std::vector<Setting*> {
			settingsCache[resolution],
			settingsCache[fps],
			settingsCache[font]
		}));
		
		pages.insert(std::make_pair(settingsPage::physics, std::vector<Setting*> {
			settingsCache[gravity],
			settingsCache[friction],
			settingsCache[jumpStrength],
			settingsCache[walkSpeed],
			settingsCache[windSpeed]
		}));

		pages.insert(std::make_pair(settingsPage::misc, std::vector<Setting*> {
			settingsCache[difficulty],
			settingsCache[month],
			settingsCache[gender]
		}));
		
		setPage(main);
	}
	
	void enableSetting(settingName setting) {
		// set the relevant y coords based on how many existing settings in this page
		// put this in the relevant page
		// this is the relatively easy part; adding settings to existing ones may be less so
		// 
		
		pages[settingsCache[setting]->getCategory()].push_back(settingsCache[setting]);
		
		// Switch to the relevant page to show off the new setting
		setPage(settingsCache[setting]->getCategory());
	}

	void hideAll() {
		visibleButtons.clear();
	}

	void setVisible(Button* b, bool visible){ 
		if(visible) visibleButtons.insert(b);
		else {
			b->reset();
			visibleButtons.erase(b);
		}
	}
	
	IntSetting::IntSetting(settingsPage page, int _get(), void _set(int), int _ystart, int width, std::string name, boundsMode _mode, int _min, int _max) :
			setWrapped(_set), getWrapped(_get),
			mode(_mode), min(_min), max(_max),
			dn_b(32, _ystart + 16, 0, 0, TILESIZE, TILESIZE),
			up_b(width, _ystart + 16, TILESIZE, 0, TILESIZE, TILESIZE) {
		displayName = name;
		yStart = _ystart;
		category = page;

		IntSetting* _this = this;
		up_b.effect = [_this](){ _this->adjust(+1); };
		dn_b.effect = [_this](){ _this->adjust(-1); };
	}
	
	StringListSetting::StringListSetting(settingsPage page, int _get(), void _set(int), int ystart, int width, std::string name, boundsMode _mode, std::vector<std::string> _optionNames) :
			IntSetting(page, _get, _set, ystart, width, name, _mode, 0, _optionNames.size()),
			optionNames(_optionNames) {
	}
	
	CheckGroupSetting::CheckGroupSetting(settingsPage page, bool _get(char), void _set(char, bool), int _ystart, std::string name, std::vector<std::pair<char, std::string>> options) :
			setWrapped(_set), getWrapped(_get) {
		displayName = name;
		yStart = _ystart;
		category = page;

		CheckGroupSetting* _this = this;
		for (int i=0; i<options.size(); ++i) {
			char id = options[i].first;
			items.insert(std::make_pair(id, CheckGroupItem (
				Button(40, yStart + 24*(i+1), 6*TILESIZE, 0, TILESIZE/2, TILESIZE/2),
				Button(40, yStart + 24*(i+1), 6*TILESIZE, 2*TILESIZE, TILESIZE/2, TILESIZE/2),
				options[i].second,
				i
			)));
			items.at(id).on_b.effect = [_this, id](){ _this->set(id, true); };
			items.at(id).off_b.effect = [_this, id](){ _this->set(id, false); };
		}
	}
			
	
	void IntSetting::adjust(int delta) {
		int prev = getWrapped();
		int newValue = prev + delta;
		if (mode == boundsMode::wrapping) {
			newValue = (newValue + max) % max;
		}
		setWrapped(newValue);
		
		if (mode == boundsMode::minMax) {
			up_b.disable(newValue >= max);
		}
		
		if (mode == boundsMode::min || mode == boundsMode::minMax) {
			dn_b.disable(newValue <= min);
		}
	}
	
	void IntSetting::setVisible(bool visible) {
		//Setting::setVisible(visible);
		settings::setVisible(&up_b, visible);
		settings::setVisible(&dn_b, visible);
	}
	
	void CheckGroupSetting::set(char id, bool set) {
		settings::setVisible(&items.at(id).on_b, !set);
		settings::setVisible(&items.at(id).off_b, set);
		
		setWrapped(id, set);
	}
	
	void CheckGroupSetting::setVisible(bool visible) {
		for (auto& kv : items) {
			if (getWrapped(kv.first)) {
				settings::setVisible(&kv.second.off_b, true);
			} else {
				settings::setVisible(&kv.second.on_b, true);
			}
		}
	}
	
	void renderSettings() {
		const std::vector<Setting*>& visibleSettings = pages[page];
		for(int i=0; i<visibleSettings.size(); ++i) visibleSettings[i]->render();
	}
	
	void setPage(settingsPage _page) {
		hideAll();
		
		page = _page;
		if (page == main) {
			for (int i=1; i<pageButtons.size(); ++i) {
				setVisible(&pageButtons[i], true);
			}
		} else {
			setVisible(&pageButtons[0], true);
		}
		// Show all the settings of this page
		for (int i=0; i < pages[page].size(); ++i) {
			pages[page][i]->setVisible(true);
		}
	
	}
	
	settingsPage getSettingsPage() {
		return page;
	}

	const std::unordered_set<Button*>& listButtons(){
		return visibleButtons;
	}
}

Button::Button(int screenX, int screenY, int tilesetX, int tilesetY, int width, int height): _x(screenX), _y(screenY) {
	sprite = new sf::Sprite(settings::buttonsTexture);
	
	_defaultSlice   = sf::IntRect(tilesetX, tilesetY,          width, height);
	_highlightSlice = sf::IntRect(tilesetX, tilesetY + height, width, height);
	_pressedSlice   = sf::IntRect(tilesetX, tilesetY+2*height, width, height);
	_disabledSlice  = sf::IntRect(tilesetX, tilesetY+3*height, width, height);
	
	sprite->setPosition(screenX,screenY);
	sprite->setTextureRect(_defaultSlice);
	
	rect = sf::IntRect(screenX, screenY, width, height);
	
	_disabled = false;
	
}

sf::Sprite* Button::getSprite() {
	return sprite;
}

void Button::highlight(){
	if(!_disabled && !_pressed) sprite->setTextureRect(_highlightSlice);
}
void Button::depress(){
	if(!_disabled) {
		sprite->setTextureRect(_pressedSlice);
		_pressed = true;
	}
}
void Button::reset(){
	sprite->setTextureRect(_disabled ? _disabledSlice : _defaultSlice);
	_pressed = false;
}
void Button::disable(bool disable){
	_disabled = disable;
	reset();
}
#include <iostream>
Powerup::Powerup(int _x, int _y, settings::settingName _name) : x(_x), y(_y), setting(_name) {
	sprite.setTexture(display::getTileset());
	settings::settingsPage category = settings::settingsCache[_name]->getCategory();
	sprite.setTextureRect(sf::IntRect( (category - 1) * TILESIZE, 0, TILESIZE, TILESIZE));
	sprite.setPosition(x * TILESIZE + SETTINGSWIDTH, y * TILESIZE);
}

sf::Sprite& Powerup::getSprite() {
	// todo animate this
	return sprite;
}
