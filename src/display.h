#ifndef DISPLAY_H
#define DISPLAY_H

#include <SFML/Graphics.hpp>

#define FONT_COUNT 4

namespace display {
	const sf::Texture& getTileset();
	void init();
	void update(sf::RenderWindow& window);
	void updateTerrainImage(int, int, int);
	
	int getGlobalScale();
	int getFps();
	void setGlobalScale(int);
	void setFps(int);

	int getWindSpeed();
	void setWindSpeed(int);
	int getMonth();
	void setMonth(int);
	int getFont();
	void setFont(int);
	
	void loadWallsTexture();
}

#endif
