#include "display.h"
#include "player.h"
#include "world.h"
#include "settings.h"
#include "audio.h"

#include <iostream>
#include <random>
#include <unordered_set>

#define BRICK_VARIANT_COUNT 5
#define CLOUD_VARIANT_COUNT 4
#define SPIKE_VARIANT_COUNT 4
#define BRICKS_PER_CLOUD 2

#define CHARHEIGHT 20
#define CHARWIDTH 12

namespace display {

	sf::RenderStates eraser(sf::BlendMode(sf::BlendMode::Factor::Zero, sf::BlendMode::Factor::Zero));

	sf::Texture tileset;
	sf::Texture newTileset;
	sf::Sprite wallStamp;
	
	sf::RenderTexture wallsTexture;
	sf::Sprite wallsSprite;
	
	std::vector<sf::RenderTexture*> backdropTextures;
	std::vector<sf::Sprite> backdropSprites;

	sf::RenderTexture windowTexture;
	sf::Sprite windowSprite;
	
	sf::Texture fontpages[FONT_COUNT];
	sf::Sprite charSprite;
	
	sf::Sprite powerupSprite;
	
	sf::Sprite hat;
	
	int scale = 1;
	int fps = 20;
	
	int month = 3;
	
	int cloudDepth = 3;
	
	bool resize = false;
	
	int getGlobalScale() {
		return scale;
	}
	
	int getFps() {
		return fps;
	}
	
	void setGlobalScale(int s) {
		if(s > 0) scale = s;
		resize = true;
	}
	
	void setFps(int f) {
		fps = f;
	}
	
	const sf::Texture& getTileset() {
		return tileset;
	}
	
	int tick = 0;
	
	int windSpeed = 1;
	void setWindSpeed(int w) {
		windSpeed = w;
	}
	int getWindSpeed() {
		return windSpeed;
	}
	
	void setMonth(int m) {
		month = m;
	}
	int getMonth() {
		return month;
	}
	
	int font = 0;
	void setFont(int f) {
		font = f;
		charSprite.setTexture(fontpages[f]);
	}
	int getFont() {
		return font;
	}
	
	void createCloudLayer() {
		backdropTextures.push_back(new sf::RenderTexture());
		int currentDepth = backdropTextures.size() - 1;
		backdropTextures[currentDepth]->create(TOTALWIDTH, TOTALHEIGHT);
		backdropTextures[currentDepth]->clear(sf::Color(0,0,0,0));

		for(int x=0; x<SCREENTILEWIDTH / BRICKS_PER_CLOUD; ++x) {
			for(int y=0; y<SCREENTILEHEIGHT / BRICKS_PER_CLOUD; ++y) {
				int chance = rand();
				if (chance % (currentDepth+2) != 0) continue;
				chance /= currentDepth + 2;
				wallStamp.setTextureRect(sf::IntRect((chance % CLOUD_VARIANT_COUNT) * TILESIZE * BRICKS_PER_CLOUD, TILESIZE*4, TILESIZE * BRICKS_PER_CLOUD, TILESIZE * BRICKS_PER_CLOUD));
				int perturbationX = chance % (TILESIZE/2);
				chance /= TILESIZE/2;
				int perturbationY = chance % (TILESIZE/2);
				wallStamp.setPosition(x*TILESIZE * BRICKS_PER_CLOUD + perturbationX - TILESIZE/4, y*TILESIZE * BRICKS_PER_CLOUD + perturbationY - TILESIZE/4);
				backdropTextures[currentDepth]->draw(wallStamp);
			}
		}
		backdropTextures[currentDepth]->display();
		
		backdropSprites.push_back(sf::Sprite());
		backdropSprites[currentDepth].setTexture(backdropTextures[currentDepth]->getTexture());
	}
	
#define TILESET_ROW 16
	
	void loadWallsTexture() {
		
		wallsTexture.create(world::getWidth() * TILESIZE, world::getHeight() * TILESIZE);
		wallsTexture.clear(sf::Color(0,0,0,0));

		for(int x=0; x<world::getWidth(); ++x) {
			for(int y=0; y<world::getHeight(); ++y) {
				unsigned char terrain = world::getTerrainAt(x,y);
				if (!terrain) continue;
				wallStamp.setTextureRect(sf::IntRect((terrain % TILESET_ROW) * TILESIZE, (terrain / TILESET_ROW) * TILESIZE, TILESIZE, TILESIZE));
				wallStamp.setPosition(x*TILESIZE, y*TILESIZE);
				wallsTexture.draw(wallStamp);
			}
		}
		wallsTexture.display();
		wallsSprite.setTexture(wallsTexture.getTexture());
		wallsSprite.setPosition(SETTINGSWIDTH, 0);
	
	}
	
	void init() {
		windowTexture.create(TOTALWIDTH + SETTINGSWIDTH, TOTALHEIGHT);
		tileset.loadFromFile("assets/tileset.png");
		newTileset.loadFromFile("assets/tileset2.png");
		
		wallStamp.setTexture(tileset);
		
		for(int i=0; i<cloudDepth; ++i) {
			createCloudLayer();
		}
		
		wallStamp.setTexture(newTileset);
		hat.setTexture(tileset);
		
		loadWallsTexture();
		
		for(int i=0; i<FONT_COUNT; ++i) {
			fontpages[i].loadFromFile("assets/font/" + std::to_string(i) + ".png");
		}
		charSprite.setTexture(fontpages[0]);
		
		powerupSprite.setTexture(tileset);
		powerupSprite.setTextureRect(sf::IntRect(0,0,TILESIZE,TILESIZE));
	}
	
	void updateTerrainImage(int x, int y, int t) {
		switch(t) {
			case 1:
			// brick
				wallStamp.setTextureRect(sf::IntRect(TILESIZE, (rand() % BRICK_VARIANT_COUNT) * TILESIZE, TILESIZE, TILESIZE));
				wallStamp.setPosition(x*TILESIZE, y*TILESIZE);
				wallsTexture.draw(wallStamp);
				break;

			case SPIKE:
				wallStamp.setTextureRect(sf::IntRect(2*TILESIZE, (rand() % SPIKE_VARIANT_COUNT) * TILESIZE, TILESIZE, TILESIZE));
				wallStamp.setPosition(x*TILESIZE, y*TILESIZE);
				wallsTexture.draw(wallStamp);
				break;
				
			case 0: {
				wallStamp.setTextureRect(sf::IntRect(0, 0, TILESIZE, TILESIZE));
				wallStamp.setPosition(x*TILESIZE, y*TILESIZE);
				wallsTexture.draw(wallStamp, eraser);
				break;
			}
		}
	}
	
	void displayText(std::string text, int originX, int originY) {
		int readPos=0;
		int writePos=0;
		while(readPos < text.length()) {
			if(text[readPos] == '\n'){
				originY += CHARHEIGHT;
				writePos = 0;
			} else {
				charSprite.setTextureRect(sf::IntRect((text[readPos]-' ') % 12 * CHARWIDTH, (text[readPos]-' ') / 12 * CHARHEIGHT, CHARWIDTH, CHARHEIGHT)); // 12 x 8 ASCII
				
				int x = originX + writePos * CHARWIDTH;
				int y = originY;
				charSprite.setPosition(x,y);
				
				windowTexture.draw(charSprite);
				++writePos;
			}
			++readPos;
		}
	}

	void update(sf::RenderWindow& window) {
		if(resize) {
			resize = false;
			int newWidth = (TOTALWIDTH+SETTINGSWIDTH) * scale;
			int newHeight = TOTALHEIGHT * scale;
			window.setSize(sf::Vector2u(newWidth, newHeight));
			window.setView(sf::View(sf::FloatRect(0,0,newWidth,newHeight)));
			windowSprite.setScale(scale, scale);
		}
	
		windowTexture.clear( (month>9 || month<2) ? sf::Color(0xc8, 0xd7, 0xd7) : sf::Color(0x5e,0xb1,0xff) );

		if (player::getDifficulty() == 0) {
			sf::RectangleShape bg(sf::Vector2f(TOTALWIDTH, TOTALHEIGHT));
			bg.setPosition(SETTINGSWIDTH,0);
			bg.setFillColor(sf::Color(255, 255, 255));
			windowTexture.draw(bg);
			displayText("Congraxulation's! Youre Winner", SETTINGSWIDTH + TOTALWIDTH/3, TOTALHEIGHT/2);
		} else if(player::getDifficulty() == 3) {
			sf::RectangleShape bg(sf::Vector2f(TOTALWIDTH, TOTALHEIGHT));
			bg.setPosition(SETTINGSWIDTH,0);
			bg.setFillColor(sf::Color(0, 0, 0));
			windowTexture.draw(bg);
			displayText("dead. :( try again?", SETTINGSWIDTH + TOTALWIDTH/2, TOTALHEIGHT/2);
		} else {
			
			for (int i=cloudDepth-1; i>=0; --i) {
				backdropSprites[i].setPosition(SETTINGSWIDTH-((i+1) * tick % (SCREENTILEWIDTH*TILESIZE)), 0);
				windowTexture.draw(backdropSprites[i]);
				backdropSprites[i].setPosition((SCREENTILEWIDTH*TILESIZE)+SETTINGSWIDTH-((i+1) * tick % (SCREENTILEWIDTH*TILESIZE)), 0);
				windowTexture.draw(backdropSprites[i]);
			}
			
			int offsetX = -player::getX() + TILESIZE*SCREENTILEWIDTH/2;
			int offsetY = -player::getY() + TILESIZE*SCREENTILEHEIGHT/2;
			
			wallsSprite.setPosition(offsetX + SETTINGSWIDTH, offsetY);
			
			windowTexture.draw(wallsSprite);
			
			sf::Sprite& playerSprite = player::getSprite();
			playerSprite.setPosition(player::getX() + offsetX + SETTINGSWIDTH, player::getY() + offsetY);
			windowTexture.draw(playerSprite);
			switch(month) {
				case 11:
					hat.setTextureRect(sf::IntRect(0,224,24,24));
					hat.setPosition(player::getX() + offsetX + SETTINGSWIDTH + 8, player::getY() + offsetY - 11);
					windowTexture.draw(hat);
					break;
				case 9:
					hat.setTextureRect(sf::IntRect(24,224,24,24));
					hat.setPosition(player::getX() + offsetX + SETTINGSWIDTH + 8, player::getY() + offsetY - 11);
					windowTexture.draw(hat);
					break;
				case 5:
					hat.setTextureRect(sf::IntRect(48 + 12*(tick%6), 244, 12, 12));
					hat.setPosition(player::getX() + offsetX + SETTINGSWIDTH - 8, player::getY() + offsetY - 12);
					windowTexture.draw(hat);
					break;
			}
			if(player::isMale()) {
				hat.setTextureRect(sf::IntRect(month == 5 ? 76 : 48, 237, 14, 7));
				hat.setPosition(player::getX() + offsetX + SETTINGSWIDTH + 17, player::getY() + offsetY + 21);
				windowTexture.draw(hat);
			}
			if(player::isFemale()) {
				hat.setTextureRect(sf::IntRect(month == 5 ? 76 : 62, 237, 14, 7));
				hat.setPosition(player::getX() + offsetX + SETTINGSWIDTH + 13, player::getY() + offsetY + 7);
				windowTexture.draw(hat);
			}
			
			for (int i=0; i<world::countPowerups(); ++i) {
				Powerup pu = world::getPowerup(i);
				pu.getSprite().setPosition(pu.getX() * TILESIZE + offsetX + SETTINGSWIDTH, pu.getY() * TILESIZE + offsetY);
				windowTexture.draw(pu.getSprite());
			}
		}
		
		sf::RectangleShape settingsBg(sf::Vector2f(SETTINGSWIDTH, TOTALHEIGHT));
		settingsBg.setPosition(0,0);
		settingsBg.setFillColor(sf::Color(0x57, 0, 0xae));
		windowTexture.draw(settingsBg);
		const std::unordered_set<Button*>& visibleButtons = settings::listButtons();
		for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr) windowTexture.draw(*(*itr)->getSprite());
		
		if (settings::getSettingsPage() == settings::main) {
			displayText("SETTINGS", 24, 24);
		} else {
			displayText(settings::getPageName(), 64, 32);
		}
		
		settings::renderSettings();
		
		windowTexture.display();
		windowSprite.setTexture(windowTexture.getTexture());
		
		window.clear(sf::Color(0,0,0));
		window.draw(windowSprite);
		window.display();
		
		tick += windSpeed;
		if(tick < 0) tick = SCREENTILEWIDTH*TILESIZE + tick;
	}
}

namespace settings {
	void IntSetting::render() const {
		display::displayText(displayName, 24, yStart);
		display::displayText(std::to_string(getWrapped()), 72, yStart + 24);
	}

	void StringListSetting::render() const {
		display::displayText(displayName, 24, yStart);
		display::displayText(optionNames[getWrapped()], 72, yStart + 24);
	}
	
	void CheckGroupSetting::render() const {
		display::displayText(displayName, 24, yStart);
		
		for(const auto& kv : items) {
			display::displayText(kv.second.displayName, 72, yStart + (kv.second.pos + 1) * 24);
		}
	
	}
}
