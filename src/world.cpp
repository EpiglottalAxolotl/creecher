#include "world.h"
#include "display.h"
#include "powerup.h"
#include "audio.h"

#include <fstream>
#include <random>

namespace player {
		void goToTile(int, int);
}

namespace world {

	unsigned char** terrainMap = NULL;
	
	std::vector<Powerup> powerups;

	int currentLevel;

	void init() {
		srand(time(NULL));

		loadLevel(0);
	}
	
	int width;
	int height;
	int spawnX;
	int spawnY;
	
	int getWidth() {
		return width;
	}
	int getHeight() {
		return height;
	}
	int getSpawnX() {
		return spawnX;
	}
	int getSpawnY() {
		return spawnY;
	}
	
	void loadLevel(int mapid) {
		if (terrainMap) {
			for(int i=0; i<height; ++i) {
				delete[] terrainMap[i];
			}
			delete terrainMap;
		}
		
		std::ifstream leveldata("assets/level/"+std::to_string(mapid));
		leveldata >> width >> height >> spawnX >> spawnY;
		
		std::ifstream tiles("assets/map/"+std::to_string(mapid), std::ios::binary);

		terrainMap = new unsigned char* [height];
		for(int i=0; i<height; ++i){
			char* typeRow = new char[width];
			tiles.read(typeRow, width);
			terrainMap[i] = (unsigned char*) typeRow;
		}
		
		// In the future: set player XY to coordinates in the level file.
		// Also, put powerups in the level file.
		
		powerups.push_back(Powerup(4, 9, settings::levelSelect));
		powerups.push_back(Powerup(22, 7, settings::musicPitch));
		
		currentLevel = mapid;
		
		display::loadWallsTexture();
		
		player::goToTile(spawnX, spawnY);
	}
	
	int getLevel() {
		return currentLevel;
	}
	
	void setTerrainAt(int x, int y, int t) {
		terrainMap[y][x] = t;
		display::updateTerrainImage(x, y, t);
	}
	
	unsigned char getTerrainAt(int x, int y) {
		if(x >= width || y >= height || x < 0 || y < 0) return 1;
		return terrainMap[y][x];
	}
	
	bool isSolid(unsigned char terrain) {
		return !!(terrain & FLAG_SOLID);
	}
	
	bool terrainIsSolid(int x, int y){
		return isSolid(getTerrainAt(x, y));
	}
	
	bool isSpike(int x, int y) {
		return (getTerrainAt(x, y) & HAZARD_MASK) == SPIKE;
	}
	
	int countPowerups() {
		return powerups.size();
	}
	
	Powerup& getPowerup(int i) {
		return powerups[i];
	}
	
	const std::vector<Powerup>& listPowerups() {
		return powerups;
	}
	
	void checkPowerups(int xPixels, int yPixels) {
		for (int i=0; i<powerups.size(); ++i) {
			if(xPixels + TILESIZE > powerups[i].getX() * TILESIZE
				&& xPixels <= powerups[i].getX() * TILESIZE + TILESIZE
				&& yPixels + TILESIZE > powerups[i].getY() * TILESIZE
				&& yPixels <= powerups[i].getY() * TILESIZE + TILESIZE
			) {
				// Collided with this powerup
				
				// Enable the corresponding setting
				settings::enableSetting(powerups[i].getSetting());
				
				// Play a sound
				// Delete the powerup
				audio::playSound(audio::soundEffect::unlock);
				powerups.erase(powerups.begin() + i);
			}
		}
	}

}
