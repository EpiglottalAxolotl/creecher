#ifndef WORLD_H
#define WORLD_H

#include "powerup.h"

#define TILESIZE 32
#define SCREENTILEWIDTH 24
#define SCREENTILEHEIGHT 16
#define TOTALWIDTH (SCREENTILEWIDTH * TILESIZE)
#define TOTALHEIGHT (SCREENTILEHEIGHT * TILESIZE)

#define SETTINGSWIDTH (8*TILESIZE)

#define FLAG_SOLID 0x01
#define HAZARD_MASK 0xcf
#define SPIKE 0x02
#define BOUNCY 0x03

namespace world {
	void init();
	unsigned char getTerrainAt(int, int);
	void setTerrainAt(int, int, int);
	
	void loadLevel(int);
	int getLevel();
	
	void checkPowerups(int, int);
	int countPowerups();
	Powerup& getPowerup(int);
	//const std::vector<Powerup>& listPowerups();
	
	int getWidth();
	int getHeight();
	int getSpawnX();
	int getSpawnY();
}

#endif
